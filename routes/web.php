<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//     return redirect()->route('login');



Auth::routes();


// rutas para todos los usuarios logeados.
route::group(['middleware'=>'todos'],function(){
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/', function () {
        return view('welcome');
    });
    // mapa
    Route::get('/centros', 'MapaController@index')->name('mapa');
});


//rutas de administrador
route::group(['middleware'=>'administrador'],function(){

    Route::get('/inicio', 'InicioController@index')->name('inicio');


});

//rutas de usuario registrado
route::group(['middleware'=>'usuario'],function(){

    Route::get('/user', 'UserController@index')->name('user');


});

// Ruta de accesso denegado
Route::get('/denegado', ['as' => 'denegado', function() {
    return view('welcome');
}]);