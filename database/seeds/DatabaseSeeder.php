<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);

        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'eath2497@gmail.com',
            'password' => bcrypt('Uniproyecto_1997'),
            'permiso' => '1',
        ]);
    }
}
